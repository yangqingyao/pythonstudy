from lxml import etree
import requests
base_url = 'https://www.mzitu.com/jiepai/'
headers = {'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36'}
response = requests.get(url=base_url, headers=headers)

# 解析内容，使用xpath解析数据
html = etree.HTML(response.text)
# 找到div下的所有标签，一步步靠近
ul_list = html.xpath('//*[@id="comments"]/ul/li')
num = 0
for li in ul_list:
    num += 1
    img_url = li.xpath('./div/p/img/@data-original')[0]
    print(img_url)
    # 发送请求下载图片
    img_response = requests.get(url=img_url, headers=headers)
    with open('./result/{}.jpg'.format(num), 'wb') as f:
        f.write(img_response.content)