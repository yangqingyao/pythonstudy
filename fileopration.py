# f=open()
# # 读取文件
# # 或者写入文件
# f.close()
# "r : 只读，文件不存在就会报错"
# "W：只写，会将原来的数据清空再写入，如果文件不存在，会创建文件夹" \
# "a : 追加写入"
# "rb: 二进制只读，文件不存在就会报错"
# "Wb：二进制只写，会将原来的数据清空再写入，如果文件不存在，会创建文件夹" \
# "ab : 二进制追加写入"
# r+ :可以读也可以写，覆盖的形式写入，abc de ----dec
# w+:

# w model gbk utf-8
# windows :gbk 务必注意编码
# mac linux :utf-8

# file = open("1.txt", 'w', encoding='utf-8')
# # print(file.encoding)
# # 写入数据
# file.write('a')
# file.write('b')
# file.close()


# file = open("1.txt", 'w', encoding='utf-8')
# # print(file.encoding)
# # 写入数据
# file.write('abc')
# file.close()

# read model
#
# file = open("1.txt", 'r', encoding='utf-8')
# # 读取数据
# content = file.read()
# print(content)
# file.close()

# a model
#
# file = open("1.txt", 'rb')
# # 读取数据bit
# content = file.read()
# # 将二进制转换为utf-8 deconde
# result = content.decode('utf-8')
# print(result)
# file.close()

# 文件不同的读取方式
file = open("1.txt", 'rb+')
# print(file.read())
# 读取方式的限定
# 读取文件的大小，英文只占一个字节，中文占3个字节
print(file.read(7)) #并不是以字节的个数为量值，以字符数为量节。
file.close()
# 需要循环读写的时候，就选用for循环来读。


