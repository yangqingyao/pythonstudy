import requests
url='http://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png'
# 发送网络请求（get post两种请求
# 请求头,本人浏览器的信息，字典形式
headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.3'}

response = requests.get(url=url, headers=headers)
# 显示拿回的信息
# 二进制返回，建议使用
print(response.content)
# print(response.content.decode('utf-8'))
# # 文本形式返回数值
# print(response.text)

# # 保存内容
# file = open('baidu.png', 'wb')
# file.write(response.content)
# file.close()

with open('baidu2.png', 'wb') as file:
    file.write(response.content)
